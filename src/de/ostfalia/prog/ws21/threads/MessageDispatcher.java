package de.ostfalia.prog.ws21.threads;

import de.ostfalia.prog.ws21.messages.Message;
import de.ostfalia.prog.ws21.messages.MessageBuffer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class MessageDispatcher {
	
	private MessageBuffer writeMessageBuffer;
	private MessageBuffer readMessageBuffer;

	private boolean threadRunning = true;
    private final int transmissionThreadTimer = 5;
    private Thread transmissionThread;


	
    public MessageDispatcher() {
    	writeMessageBuffer = new MessageBuffer();
    	 readMessageBuffer = new MessageBuffer();

         transmissionThread = new Thread(){
        	 @Override
        	    public void run(){
        	    	TransmissionRoutine();
        	    }
         };
         transmissionThread.start();
    }
    
    public void stopThreads() {
    	threadRunning = false;
    }
    
    private void TransmissionRoutine()
    {
        threadRunning = true;
        while (threadRunning)
        {
                try {
                    RxRoutine();
                    TxRoutine();
                    DispatchRoutine();
					Thread.sleep(transmissionThreadTimer);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            
        }
    }
    
    private void TxRoutine()
    {
    	Message msg = GetNextTxMessage();
        if (msg != null) {
        	writeMessageBuffer.AddMessage(msg);         
        }        
    }
    
    private void RxRoutine()
    {
        Message msg = GetNextRxMessage();
        if (msg != null) {
            readMessageBuffer.AddMessage(msg);         
        }
    }
    
    private Message GetNextTxMessage()
    {
        return writeMessageBuffer.GetNext();
    }
    
    private Message GetNextRxMessage()
    {
        return writeMessageBuffer.GetNext();
    }
    
    public void loadRxBuffer(Message msg)
    {

        readMessageBuffer.AddMessage(msg);
    }
    
    public void loadTxBuffer(String str)
    {
    	Message _msg = new Message();
    	_msg.setContent(str);
    	
        writeMessageBuffer.AddMessage(_msg);
    }
    
   

    private void DispatchRoutine()
    {
        Message msg = GetNextTxMessage();
        Message msg2 = GetNextRxMessage();
        if (msg != null)
        {
            DispatchConsole(msg);
        }
        if (msg2 != null)
        {
            DispatchConsole(msg2);
        }
    }
    
    public String getInputConsole() {
    	
    	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = "";
		try {
			s = br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return s;
    }
    

    private void DispatchConsole(Message msg)
    {
        System.out.println(msg.getContent());
    }
    
    public String GetCommandConsole()
    {
    	Message msg = new Message();
    	msg.setContent(getInputConsole());
    	
    	loadRxBuffer(msg); 
        return msg.getContent();
    }
}
