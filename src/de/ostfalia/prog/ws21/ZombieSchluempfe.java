package de.ostfalia.prog.ws21;

import de.ostfalia.prog.ws21.enums.Richtung;
import de.ostfalia.prog.ws21.exceptions.FalscheSpielerzahlException;
import de.ostfalia.prog.ws21.exceptions.UngueltigePositionException;
import de.ostfalia.prog.ws21.figuren.Figur;
import de.ostfalia.prog.ws21.figuren.Schlumpf;
import de.ostfalia.prog.ws21.enums.Farbe;

import de.ostfalia.prog.ws21.interfaces.IZombieSchluempfe;


public class ZombieSchluempfe implements IZombieSchluempfe {
	
	
	protected PlayField playfield;
	protected Schlumpf currentSchlumpf;
	protected Figur currentFigur;

	
	public ZombieSchluempfe(Farbe ... farben) throws FalscheSpielerzahlException, UngueltigePositionException  {	
			playfield = new PlayField(farben);
	}
	public ZombieSchluempfe(String stellung, Farbe ... farben) throws FalscheSpielerzahlException, UngueltigePositionException  {	
			playfield = new PlayField(stellung, farben);				
	}
	
	//bewegt die Figur um @param augenzahl in Richtung Konsoleneingabe
	public boolean bewegeFigur(String figurName, int augenzahl) {
		return moveFigure(figurName, augenzahl);
	}
	
	//bewegt die Figur um @param augenzahl in Richtung @param richtung
	public boolean bewegeFigur(String figurName, int augenzahl, Richtung richtung) {
		return moveFigure(figurName, augenzahl, richtung);
	}

	public boolean moveFigure(String figurName, int augenzahl, Richtung richtung)  {
		Farbe gewinner = gewinner();

		if(gewinner == null) {
			
			playfield.iO.print("Choose your smurf");
			
			currentFigur = playfield.returnPlayerByFigurName(figurName);
	
			currentFigur.checkRestrictionsAndMove(augenzahl, playfield.iO, richtung, currentFigur.actualField, playfield.fieldList);

			playfield.nextTurn();
		} else {
			playfield.iO.print("Winner is: " + gewinner.toString());
		}
		
		
		return true;
	}
	
	public boolean moveFigure(String figurName, int augenzahl) {
		Farbe gewinner = gewinner();

		if(gewinner == null) {
			playfield.iO.print("Choose your smurf");
			currentFigur = playfield.returnPlayerByFigurName(figurName);
				
			currentFigur.checkRestrictionsAndMove(augenzahl, playfield.iO, Richtung.WEITER, currentFigur.actualField, playfield.fieldList);

			playfield.nextTurn();
		} else {
			playfield.iO.print("Winner is: " + gewinner.toString());
		}
		
		
		return true;
	}
	

	//gibt die Feldnummer der Figur zurück
	public int getFeldnummer(String name) {
		return playfield.returnPlayerByFigurName(name).actualField.pos;
	}


	//gibt zurück ob eine Figur ein Zombie ist
	public boolean istZombie(String name) {
		return playfield.returnPlayerByFigurName(name).getisZombie();
	}


	//gibt die Farbe die am Zug ist zurück
	public Farbe getFarbeAmZug() {
		return playfield.zug.spielerFarbe;
	}

	//gibt den Gewinner zurück
	public Farbe gewinner() {

		return playfield.returnWinner();
	}

	
}
