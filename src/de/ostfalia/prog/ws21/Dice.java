package de.ostfalia.prog.ws21;



public class Dice {

	final int[] diceSide = { 1, 2, 3, 4, 5, 6, 7 };
	
	//calculate random number between Min = 0 (index starts at 0) and DiceSide which is the length of the enum 
	
	private int returnRandomSide() {
		
		int min = 1;
		int max = 7;
		
		
		return min + (int)(Math.random() * ((max - min) + 1));
	}
	
	
	// Return Side/Eyes
	
	public int rollDice() {

		return returnRandomSide();
	}
}
