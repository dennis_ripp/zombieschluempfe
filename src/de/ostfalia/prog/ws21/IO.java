package de.ostfalia.prog.ws21;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import de.ostfalia.prog.ws21.enums.Richtung;
import de.ostfalia.prog.ws21.threads.MessageDispatcher;

public class IO {

	String args;
	Map<String, String> commands;
	Scanner keyboard;
	MessageDispatcher messageDispatcher;
	

	
	public IO() {
		messageDispatcher = new MessageDispatcher();
	}
	
	
	/**
	 * Liest Tastatur Input ein und vergleich mit allen Werten im Enum Richtung 
	 * @return gibt das Enum zurück wenn Wert übereinstimmt
	 */
	public Richtung takeRoute() {

		print("\nFork ahead \n ABZWEIGEN or WEITER ?");
		
		this.args = takeInput();
		
		for(int i = 0; i < Richtung.values().length; i++) {
			
			String buf = Richtung.values()[i].toString();
			
			if(buf.equals(args)) {
				return Richtung.values()[i];
			}
		}	
		return null;	
	}
	
	public void print(String str) {
		messageDispatcher.loadTxBuffer(str);
	}
	
	public  String takeInput() {
	
		return messageDispatcher.GetCommandConsole();
	}
	
	
	
	
	public void cDic()
	{
		commands = new HashMap<String, String>();
		commands.put("h", "help");
		commands.put("h", "help");
		commands.put("h", "help");
		commands.put("h", "help");
		commands.put("h", "help");
		commands.put("h", "help");

		System.out.println(commands.get("dog"));
	}
}
