package de.ostfalia.prog.ws21;

import java.util.HashMap;
import java.util.Map;

import de.ostfalia.prog.ws21.enums.Farbe;
import de.ostfalia.prog.ws21.felder.Field;
import de.ostfalia.prog.ws21.figuren.Schlumpf;

public class Spieler  {

	Schlumpf[] schluempfe;
	final int smurfCnt = 4;
	Farbe spielerFarbe;
	boolean isActive;
	Map<String, Schlumpf> commands;


	public Spieler(Farbe farbe, boolean isActive, Field field){
		this.spielerFarbe = farbe;
		this.isActive = isActive;
		this.schluempfe = new Schlumpf[smurfCnt];
		this.commands = new HashMap<String, Schlumpf>();
		
		//erstellt die 4 schlümpfe pro spieler
		createSmurfs(field);
		
		createPlayerCommands(this.commands);
	}
	
	
	//erstellt die 4 schlümpfe pro spieler
	private void createSmurfs(Field field) {
		for (int i = 0; i < smurfCnt; i++) {
			schluempfe[i] = new Schlumpf(this.spielerFarbe, i, field);
		}
	}
	
	private void createPlayerCommands(Map<String, Schlumpf> smurfMap)  {
		for (int i = 0; i < this.schluempfe.length; i++) {
			 smurfMap.put(this.schluempfe[i].iD, schluempfe[i]);
		}
	}
	
	public Schlumpf returnSmurf(String name) {
		return this.commands.get(name);
	}
	
}
