package de.ostfalia.prog.ws21.exceptions;

public class WiederholteFarbenException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	 // Parameterless Constructor
    public WiederholteFarbenException() {}

    // Constructor that accepts a message
    public WiederholteFarbenException(String message) {
       super(message);
    }
}
