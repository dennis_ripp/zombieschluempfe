package de.ostfalia.prog.ws21.exceptions;

public class UngueltigePositionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UngueltigePositionException() {}

    // Constructor that accepts a message
    public UngueltigePositionException(String message)
    {
       super(message);
    }

}
