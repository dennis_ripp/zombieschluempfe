package de.ostfalia.prog.ws21.exceptions;

public class FalscheSpielerzahlException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 // Parameterless Constructor
    public FalscheSpielerzahlException() {}

    // Constructor that accepts a message
    public FalscheSpielerzahlException(String message)
    {
       super(message);
    }

}
