package de.ostfalia.prog.ws21.felder;

import java.util.ArrayList;
import java.util.List;

import de.ostfalia.prog.ws21.IO;
import de.ostfalia.prog.ws21.enums.Farbe;
import de.ostfalia.prog.ws21.enums.Richtung;
import de.ostfalia.prog.ws21.figuren.Figur;
import de.ostfalia.prog.ws21.figuren.Schlumpf;

public abstract class Field {

	/**
	* Typ des Feldes aus dem Enum Felder
	*/
	/**
	 * Auflistung der Spieler, die sich in/auf diesem Feld befinden
	 */
	public List<Figur> isHere;
	/**
	 * 	//Iteration der Felder
	 */
	public int pos;
	public Farbe gewinner;
    protected Field next;
	protected Field nextPath;

	
	public Field(int posNo) {
		this.next = null;
		this.pos = posNo;
		this.isHere = new ArrayList<Figur>();

	}
	
	public Field getnext() {
		return this.next;
	}
	
	//do event for special fields
	public abstract void doFieldEvent(IO iO, int augenzahl, Richtung richtung, List<Field> fields);
	
	public abstract Farbe doFieldEvent();

			
	public void setnext(Field field) {
		this.next = field;
	}
	
	public void checkForFieldEvent(IO iO, int augenzahl, Richtung richtung, List<Field> fields) {
		if(!isHere.isEmpty()) {
			doFieldEvent(iO, augenzahl, richtung, fields);
		}
	}
	
	public List<Figur> returnPlayerSmurfs() {	
		List<Figur> playerSmurfs = new ArrayList<Figur>();

		for (Figur smurfHere : this.isHere) {
			if(smurfHere instanceof Schlumpf) {
				playerSmurfs.add(smurfHere);
			}
		}
		
		return  playerSmurfs;
	}
	
	public List<Figur> returnZombieSmurfs() {		
		List<Figur> zombieSmurfs = returnPlayerSmurfs();
		
		for (Figur smurfHere : this.isHere) {
			if(smurfHere instanceof Schlumpf) {
				if(smurfHere.getisZombie()) {
					zombieSmurfs.add(smurfHere);
				}
			}
		}
		
		return  zombieSmurfs;	
	}
	protected void warpTo(Figur figur, Field destination) {
		figur.actualField.isHere.remove(figur);
		figur.actualField =  destination;
		figur.actualField.isHere.add(figur);
		figur.fieldNumber = figur.actualField.pos;
	}
	
	public String toString() {
		return this.pos + " " + this.getClass().getSimpleName();
	}
}
