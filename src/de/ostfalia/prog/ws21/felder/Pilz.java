package de.ostfalia.prog.ws21.felder;

import java.util.List;

import de.ostfalia.prog.ws21.IO;
import de.ostfalia.prog.ws21.enums.Farbe;
import de.ostfalia.prog.ws21.enums.Richtung;
import de.ostfalia.prog.ws21.figuren.Figur;
import de.ostfalia.prog.ws21.figuren.Schlumpf;

public class Pilz extends Field {

	public Pilz(int posNo, Field nextField) {
		super(posNo);
		this.next = nextField;
	}

	public void doFieldEvent(IO iO, int augenzahl, Richtung richtung, List<Field> fields) {
		if(augenzahl == 0) {
			for (Figur figur : isHere) {
				if(figur instanceof Schlumpf) {
					if(figur.getisZombie()) {
						//schlumpf heilen
						figur.setisZombie(false);
						//schlumpf zum startfeld befördern
						for(Field field : fields) {
							if(field instanceof Startfeld) {
								field.warpTo(figur, field);
								return;
							}
						}
					}
				}
			}
		}
	}

	@Override
	public Farbe doFieldEvent() {
		// TODO Auto-generated method stub
		return null;
	}


}
