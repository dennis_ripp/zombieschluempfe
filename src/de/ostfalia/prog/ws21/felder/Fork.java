package de.ostfalia.prog.ws21.felder;

import java.util.List;

import de.ostfalia.prog.ws21.IO;
import de.ostfalia.prog.ws21.enums.Farbe;
import de.ostfalia.prog.ws21.enums.Richtung;

public class Fork extends Field {
	
	private Field alternatePath;

	public Fork(int pos, Field alternateRoute, Field nextField) {	
		super(pos);
		this.alternatePath = alternateRoute;
		this.nextPath = nextField;
		this.next = nextField;

	}

	public void doFieldEvent(IO iO, int augenzahl, Richtung inputParam, List<Field> fields) {
		iO.print("Fork ahead");
		Richtung richtung;
		
		if(inputParam != null) {		
			richtung = inputParam;
		} else {
			richtung = iO.takeRoute();
		}

		switch(richtung) {
			case ABZWEIGEN: {
				this.next = this.alternatePath;
				break;
			}
			default: {
				this.next = this.nextPath;
				break;
			}
		}
	}

	
	@Override
	public Farbe doFieldEvent() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Field getalternatePath() {
		return this.alternatePath;
	}
}

	
	
