package de.ostfalia.prog.ws21.felder;

import java.util.List;

import de.ostfalia.prog.ws21.IO;
import de.ostfalia.prog.ws21.enums.Farbe;
import de.ostfalia.prog.ws21.enums.Richtung;
import de.ostfalia.prog.ws21.figuren.Figur;

public class Dorf extends Field {

	Farbe gewinner;
	
	public Dorf(int posNo) {
		super(posNo);
		this.next = this;
		// TODO Auto-generated constructor stub
	}

	@Override
	public Farbe doFieldEvent() {
		//ermittelt den gewinner
		int[] figurCnt = new int[4];
		
		//zählt den counter der jeweiligen Farbe hoch pro Spielerfigur mit Farbpräfix
		for(Figur spieler : this.isHere) {
			switch(spieler.figurFarbe) {
				case ROT: 
					figurCnt[Farbe.ROT.ordinal()]++;
					break;
				case BLAU: 
					figurCnt[Farbe.BLAU.ordinal()]++;
					break;
				case GELB: 
					figurCnt[Farbe.GELB.ordinal()]++;
					break;
				case GRUEN: 
					figurCnt[Farbe.GRUEN.ordinal()]++;
					break;
				
				default: break;
			}
		}
			
		//gibt die Gewinnerfarbe zurück wenn ein Element des Arrays = 4 ist
		for (int i = 0; i < figurCnt.length; i++) {
			if (figurCnt[i] == 4) {
				return Farbe.values()[i];
				
			}
		}
		return null;
	}

	@Override
	public void doFieldEvent(IO iO, int augenzahl, Richtung richtung, List<Field> fields) {
		// TODO Auto-generated method stub
		
	}
}
