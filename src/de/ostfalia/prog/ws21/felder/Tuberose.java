package de.ostfalia.prog.ws21.felder;

import java.util.List;

import de.ostfalia.prog.ws21.IO;
import de.ostfalia.prog.ws21.enums.Farbe;
import de.ostfalia.prog.ws21.enums.Richtung;
import de.ostfalia.prog.ws21.figuren.Figur;
import de.ostfalia.prog.ws21.figuren.Schlumpf;

public class Tuberose extends Field {

	public Tuberose(int pos) {
		super(pos);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doFieldEvent(IO iO, int augenzahl, Richtung richtung, List<Field> fields) {
		for (Figur figur : isHere) {
			if(figur instanceof Schlumpf) {
				if(figur.getisZombie()) {
					figur.setisZombie(false);
				}
			}
		}
	}

	@Override
	public Farbe doFieldEvent() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
