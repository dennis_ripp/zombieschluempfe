package de.ostfalia.prog.ws21;

import de.ostfalia.prog.ws21.enums.Farbe;
import de.ostfalia.prog.ws21.exceptions.FalscheSpielerzahlException;
import de.ostfalia.prog.ws21.exceptions.UngueltigePositionException;
import de.ostfalia.prog.ws21.exceptions.WiederholteFarbenException;
import de.ostfalia.prog.ws21.felder.Field;
import de.ostfalia.prog.ws21.felder.Dorf;
import de.ostfalia.prog.ws21.felder.*;

import de.ostfalia.prog.ws21.figuren.Figur;
import de.ostfalia.prog.ws21.figuren.Fliege;
import de.ostfalia.prog.ws21.figuren.Oberschlumpf;
import de.ostfalia.prog.ws21.figuren.Schlumpf;
import de.ostfalia.prog.ws21.figuren.Schlumpfine;
import de.ostfalia.prog.ws21.threads.MessageDispatcher;

import java.util.ArrayList;
import java.util.List;

public class PlayField {

	private final int fieldCount = 37;
	private final int maxSpielerAnzahl = 4;
	private int cnt = 0;
	private Spieler[] spieler;
	private int activeSpielerAnzahl;

	protected Spieler zug;
	protected Oberschlumpf doc;
	protected Fliege bzz;
	protected Schlumpfine schlumpfine;
	protected MessageDispatcher messageDispatcher;
	protected IO iO;

	public List<Figur> npcs;
	public List<Field> fieldList;
	public List<Spieler> activePlayer;
	public List<Spieler> turnList;
	public Farbe gewinner;
	
	
	public PlayField(Farbe... farben) throws FalscheSpielerzahlException, UngueltigePositionException {
		this(null, farben);
	}
	
	public PlayField(String stellung, Farbe ... farben) throws FalscheSpielerzahlException, UngueltigePositionException  {
			checkPlayerCount(farben);
			checkColors(farben);
			
			activePlayer = new ArrayList<Spieler>();
			turnList = new ArrayList<Spieler>();
			npcs = new ArrayList<Figur>();
			fieldList = new ArrayList<Field>();
			this.messageDispatcher = new MessageDispatcher();
			
			this.activeSpielerAnzahl = farben.length;
			this.spieler = new Spieler[maxSpielerAnzahl];
			
			this.iO = new IO();
			
			
			//alle Felder erstellen
			createFields();
			
			createSpecialFields();
			
			//alle Spieler erstellen
			createPlayers(fieldList.get(0));
			
			//alle NPCs erstellen
			createNPCs(this.fieldList.get(20), this.fieldList.get(29), this.fieldList.get(1));
			
			//setzt die StartPosition
			setStartPosition(this.fieldList);
		
			//deaktiviert Spieler, dessen Farben nicht als Parameter mitgegeben wurde
			activatePlayers(farben);
			
			//setzt die FeldNummer der inactiven Spieler auf -1
			setUnactivePlayersFieldNo();
		
			//setzt die Figuren gemäß dem Parameter Stellung
			setStellung(stellung);
			
			
			//fügt die aktiven Spieler der aktiven Spieler Liste hinzu
			addActivePlayerToList();
			
			//sortiert die Spieler in eine Liste nach Reihenfolge der mitgegebenen Farben
			setOrderOfTurns(farben);
				
			//setzt den Startzug auf den Spieler der Turnliste 0tes Element
			this.zug = turnList.get(0);
			
			iO.print("This is the turn of " + zug.spielerFarbe.toString());
	}
			

	private void setOrderOfTurns(Farbe... farben) {
		for (int i = 0;  i < farben.length; i++) {
			turnList.add(returnFigureByColor(farben[i]));
		}
	}

	private void addActivePlayerToList() {
		for (int i = 0; i < this.spieler.length; i++) {
			if(this.spieler[i].isActive) {
				activePlayer.add(spieler[i]);
			}
		}
	}

	private void setStellung(String buf) throws UngueltigePositionException {
		if(buf != null && !buf.equals("")){
			String[] buffArr = buf.split(", ");			
			
			for (String str : buffArr) {
				setStartPosition(str);
			}
		}
	}
	
	//Setzt die Startposition und isZombie gemäß dem Parameter "Stellung" 
	private void setStartPosition(String figurStr) throws UngueltigePositionException, WiederholteFarbenException {
		String[] buff =  figurStr.split(":");
		Figur buffFigur = returnPlayerByFigurName(buff[0]);
		
		if(buffFigur == null) {
			throw new WiederholteFarbenException();
		}
		
		int fieldNo = Integer.parseInt(buff[1]);
		
		if(buff.length == 3 && buff[2].equals("Z")) {
			buffFigur.setisZombie(true);
		}
		
		checkStellung(buffFigur, fieldNo);

		
		this.fieldList.get(fieldNo).isHere.add(buffFigur);
		buffFigur.actualField = this.fieldList.get(fieldNo);
		buffFigur.fieldNumber = fieldNo;
	}
	
	//Fügt der isHere List der Feldern die Schlümpfe der aktiven Spieler hinzu
	private void setStartPosition(List<Field> field) {
		for (Spieler currentPlayer : this.spieler) {
			if (currentPlayer.isActive) {
				for(int i = 0; i < currentPlayer.schluempfe.length; i++) {
						currentPlayer.schluempfe[i].actualField = field.get(currentPlayer.schluempfe[i].fieldNumber);
						field.get(currentPlayer.schluempfe[i].fieldNumber).isHere.add(currentPlayer.schluempfe[i]);
				}
			}
		}
	}
	
	//erhöht den Zähler und wechselt so den Zug alternierend zu den aktiven Spielern
	protected void nextTurn() {
		cnt++;
		this.zug = this.turnList.get(cnt % this.activeSpielerAnzahl);
		iO.print("This is the turn of " + zug.spielerFarbe.toString());
	}

	
	//erstellt alle Felder erstmal als normale wege
	private void createFields() {
		//create each field for the for the playfield ( i = 0-36 )
		for (int i = 0; i < 37; i++) {
			Weg weg = new Weg(i);
			this.fieldList.add(weg);
		}
		for (int i = 0; i < 36; i++) {			
			this.fieldList.get(i).setnext(this.fieldList.get(i+1));
		}
	}
	/*
	Feldfolge................:
		 * 
		 *  0-->1-->2-->3-->4-->5-->6-->7-->15-->16-->17-->18-->19-->20-->21-->22
		 *      |       |                   |                                   |
		 *      |       |                   <---------------                    |           
		 *      |       |                                  |                    |  
		 *      |       --->8-->9-->10-->(11)-->12-->13-->14                    |
		 *      |                                                               |    
		 *      |                       -->36 (Dorf)                            |    
		 *      |                       |                                       |    
		 *      <--35<--34<--33<--32<--31<--30<--29<--28<--27<--26<--25<--24<--23
		 *      
		 *      */
	
	private void createSpecialFields() {
		//create each field for the for the playfield ( i = 0-36 )
		
		this.fieldList.set(0, new Startfeld(0, this.fieldList.get(1)));
		this.fieldList.set(36, new Dorf(36));
		
		connectPaths(35, 1);

		this.fieldList.set(24, new Pilz(24, returnFieldObj(25)));
		connectPaths(23, 24);

		this.fieldList.set(11, new Tuberose(11));
		connectPaths(10, 11);
		connectPaths(11, 12);

		this.fieldList.set(27, new Fluss(27, this.fieldList.get(28)));
		this.fieldList.set(26, new Fluss(26, this.fieldList.get(27)));
		this.fieldList.set(25, new Fluss(25, this.fieldList.get(26)));
		connectPaths(24, 25);


		this.fieldList.set(17, new Fluss(17, this.fieldList.get(18)));
		this.fieldList.set(16, new Fluss(16, this.fieldList.get(17)));		
		connectPaths(15, 16);

		
		this.fieldList.set(31, new Fork(31, this.fieldList.get(36), this.fieldList.get(32)));
		connectPaths(30, 31);

		this.fieldList.set(3, new Fork(3, this.fieldList.get(8),this.fieldList.get(4)));
		connectPaths(2, 3);
		connectPaths(7, 15);

	}
	
	private void connectPaths(int first, int second) {
		returnFieldObj(first).setnext(returnFieldObj(second));

	}
	
	private Field returnFieldObj(int no) {
		return this.fieldList.get(no);
	}

	//erstellt alle Spieler und setzt die Aktivität erstmal auf false
	private void createPlayers(Field field) {
		//create Players by the amount of given colors
		boolean active = false;
		for (int i = 0; i < this.maxSpielerAnzahl; i++) {
			this.spieler[i] = new Spieler(Farbe.values()[i], active, field);
		}
	}	

	//erstellt alle NPCs die im Enum vorhanden sind
	private void createNPCs(Field ...fields) {
		bzz = new Fliege(fields[0]);
		doc = new Oberschlumpf(fields[1]);
		schlumpfine = new Schlumpfine(fields[2]);
		
		this.npcs.add(schlumpfine);
		this.npcs.add(bzz);
		this.npcs.add(doc);
	}
	
	//durchsucht die IDs aller Spieler und NPCs nach dem @param figurName
	public Figur returnPlayerByFigurName(String figurName){
		if(returnFigureByName(figurName) != null) {
			return returnFigureByName(figurName);
		}
			
		if(returnNPCByFigurName(figurName) != null) {
			return returnNPCByFigurName(figurName);
		}
		
		System.out.println("No figure found");	
		
		return null;
	}

	//durchsucht die IDs aller Spieler nach dem @param figurName
	private Figur returnFigureByName(String figurName) {
		//nested loop to look for schlumpf in each player with matching id
		for (int i = 0; i < this.spieler.length; i++) {
			Schlumpf currentSchlumpf = this.spieler[i].commands.get(figurName);
			if(currentSchlumpf != null) {
				return currentSchlumpf;
			}
		}
		return null;
	}
	
	//gibt das Spielerobjekt zurück welches mit der @param farbe übereinstimmt
	private Spieler returnFigureByColor(Farbe farbe ) {
		//nested loop to look for schlumpf in each player with matching id
		for (int i = 0; i < this.activePlayer.size(); i++) {
				if (this.activePlayer.get(i).spielerFarbe == farbe) {
					return this.activePlayer.get(i);
				}
		}
		return null;
	}

	//durchsucht die IDs aller NPCs nach dem @param figurName
	private Figur returnNPCByFigurName(String figurName) {
		//looking for npcs with matching id
		for(Figur target : this.npcs) {
			if (target.iD.equals(figurName)) {
				return target;
			}	
		}
		return null;
	}

	//setzt die FeldNummer der inactiven Spieler auf -1
	private void setUnactivePlayersFieldNo() {
		for (Spieler currerntSpieler : this.spieler) {
			if (!currerntSpieler.isActive) {
				for(int i = 0; i < currerntSpieler.schluempfe.length; i++) {
						currerntSpieler.schluempfe[i].fieldNumber = -1;
				}
			}
		}
	}
	//aktiviert alle Spieler, dessen @param farbes mitgegeben wurde
	private void activatePlayers(Farbe ... farbes) {
		for (Spieler currentSpieler : this.spieler) {
			for(int i = 0; i < farbes.length; i++) {
				if(currentSpieler.spielerFarbe == farbes[i]) {
					currentSpieler.isActive = true;
				}
			}
		}
	}
	

	private void checkStellung(Figur figur, int pos) throws UngueltigePositionException {
		
		
		if(pos < 0 || pos > this.fieldCount - 1) {
			throw new UngueltigePositionException();
		}
		
		if(figur instanceof Schlumpfine) {
			
		}
		
		if(figur instanceof Schlumpf) {
			if(figur.getisZombie()) {
				
			}
		}
		
		if(figur instanceof Fliege) {
				
		}
		
		if(figur instanceof Oberschlumpf) {
			
		}	
	}
	
	private void checkPlayerCount(Farbe[] farben) throws FalscheSpielerzahlException {
	     if(farben.length < 1 || farben.length > 4) {
	          throw new FalscheSpielerzahlException();
	     }	
	}
	
	private void checkColors(Farbe...farben) throws WiederholteFarbenException {
		int[] figurCnt = new int[4];
		
		//zählt den counter der jeweiligen Farbe hoch pro Spielerfigur mit Farbpräfix
		for(Farbe farbe : farben)
			switch(farbe) {
				case ROT: 
					figurCnt[Farbe.ROT.ordinal()]++;
					break;
				case BLAU: 
					figurCnt[Farbe.BLAU.ordinal()]++;
					break;
				case GELB: 
					figurCnt[Farbe.GELB.ordinal()]++;
					break;
				case GRUEN: 
					figurCnt[Farbe.GRUEN.ordinal()]++;
					break;
				
				default: break;
		}
		
			
		//gibt die Gewinnerfarbe zurück wenn ein Element des Arrays = 4 ist
		for (int i = 0; i < figurCnt.length; i++) {
			if (figurCnt[i] > 1) {
				throw new WiederholteFarbenException();	
			}
		}	
	}
	
	public Farbe returnWinner() {
		
		for (Field dorf : this.fieldList) {
			if(dorf instanceof Dorf) {
				return dorf.doFieldEvent();
			}
		}
		return null;
	}
	
	public String toString() {
		StringBuilder str = new StringBuilder();
		
		for(Spieler spieler : activePlayer) {
			for(Schlumpf schlumpf: spieler.schluempfe) {
				str.append(schlumpf.toString() + "\t");

			}
		str.append("\n");
		}
		
		return str.toString();
	}
}
