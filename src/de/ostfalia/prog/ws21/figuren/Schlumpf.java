package de.ostfalia.prog.ws21.figuren;

import java.util.List;

import de.ostfalia.prog.ws21.IO;
import de.ostfalia.prog.ws21.enums.Farbe;
import de.ostfalia.prog.ws21.enums.Richtung;
import de.ostfalia.prog.ws21.felder.Dorf;
import de.ostfalia.prog.ws21.felder.Field;
import de.ostfalia.prog.ws21.felder.Fluss;
import de.ostfalia.prog.ws21.felder.Pilz;

public class Schlumpf extends Figur {

	String nameIndex;

	public Schlumpf(Farbe farbe, int no, Field current) {
		super(farbe, current);
		
		this.figurFarbe = farbe;
		this.setisZombie(false);
		this.fieldNumber = 0;
		
		this.nameIndex = String.valueOf((char)('A' + no));
		this.iD = this.figurFarbe + "-" + this.nameIndex;
		
		
	}
	public String toString() { 
		String isZombieStr = (getisZombie()?":Z":"");
		
		return (this.figurFarbe + "-" + this.nameIndex + ":" + this.actualField.pos + isZombieStr);
		
	}
	@Override
	public boolean checkRestrictionsAndMove(int augenzahl, IO iO, Richtung richtung, Field field, List<Field> fields) {
		Field destination = this.returnFieldDestination(augenzahl, richtung, field);
		
		if(checkFlyDorf(augenzahl, richtung, field)) return false;
		if(!(isRestricted(destination))) {
			this.move(augenzahl, iO, richtung, field, fields);
			return true;
		} else {
			return false;
		}
	}
	
	
	@Override
	public boolean move(int augenzahl, IO iO, Richtung richtung, Field field, List<Field> fields) {
		if(augenzahl > 0) {
		//	if(checkFlyDorf(augenzahl, richtung)) {
				this.goNextField(iO, augenzahl, richtung, fields);
				this.triggerNPCEventOnField(iO);
				this.doNPCEvent(iO, augenzahl);
				this.actualField.doFieldEvent(iO, augenzahl, richtung, fields);
		
				move(--augenzahl, iO, richtung, this.actualField, fields);
			
		} else {
			this.actualField.doFieldEvent(iO, augenzahl, richtung, fields);
		}
		return true;
	}

	
	@Override
	public void doNPCEvent(IO iO, int augenzahl) {
		if(this.getisZombie()) {
			if(this.actualField instanceof Pilz) {
				
			}
		}
	}
	@Override
	protected boolean isRestricted(Field field) {
		
		if(this.getisZombie()) {
			if(field instanceof Dorf) {
				return true;
			}
		} else {
			if(field instanceof Fluss) {
				return true;
			}
		}
			
		return false;
	}

}
