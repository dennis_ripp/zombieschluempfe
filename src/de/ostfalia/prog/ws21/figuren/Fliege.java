package de.ostfalia.prog.ws21.figuren;

import java.util.List;

import de.ostfalia.prog.ws21.IO;
import de.ostfalia.prog.ws21.enums.Farbe;
import de.ostfalia.prog.ws21.enums.Richtung;
import de.ostfalia.prog.ws21.exceptions.UngueltigePositionException;
import de.ostfalia.prog.ws21.felder.Dorf;
import de.ostfalia.prog.ws21.felder.Field;
import de.ostfalia.prog.ws21.felder.Pilz;
import de.ostfalia.prog.ws21.felder.Tuberose;

public class Fliege extends Figur {

	
	public Fliege(Field current) {
		this(null, current);
	}
	
	public Fliege(Farbe farbe, Field current) {
		super(farbe, current);
		
		this.iD = "Bzz";
		this.fieldNumber = 20;
		current.isHere.add(this);
		this.setisZombie(true);
	}
	@Override
	public boolean checkRestrictionsAndMove(int augenzahl, IO iO, Richtung richtung, Field field, List<Field> fields) {
			this.move(augenzahl, iO, richtung, field, fields);
			return true;
	}
	
	@Override
	public boolean move(int augenzahl, IO iO, Richtung richtung, Field feld, List<Field> fields) {
		if(augenzahl > 0) {
			Field destination = returnFieldDestination(augenzahl, richtung);
			if(!(isRestricted(destination))) {
				this.warpTo(destination);
				this.triggerNPCEventOnField(iO);
				this.doNPCEvent(iO, augenzahl);
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
	
	
	public String toString() { 
		StringBuilder str = new StringBuilder();
		String isZombieStr = (getisZombie()?":Z":"");
		
		str.append(this.iD);
		str.append(":");
		str.append(this.fieldNumber);
		str.append(isZombieStr);

		return str.toString();
	}

	public void doNPCEvent(IO iO, int augenzahl) {
		for(Figur schlumpf : this.actualField.isHere) {
			if(!(this.actualField instanceof Pilz)) {
				if(schlumpf instanceof Schlumpf) {
					if(!schlumpf.getisZombie()) {
						schlumpf.setisZombie(true);
						iO.print("bzz infected: " + schlumpf.toString());
					}	
				}
			}
		}
		
	}

	@Override
	protected boolean isRestricted(Field field)  {
		if(field instanceof Dorf) {
			return true;
		}
		if(field instanceof Tuberose) {
			return true;
		}
		for(Figur figur :field.isHere) {
			if(figur instanceof Oberschlumpf) {
				return true;
			} 
			if(figur instanceof Schlumpfine) {
				return true;
			} 
		}
		return false;
	}
}
