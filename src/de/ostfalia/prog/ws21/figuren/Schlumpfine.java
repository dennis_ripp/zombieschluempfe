package de.ostfalia.prog.ws21.figuren;

import java.util.List;

import de.ostfalia.prog.ws21.IO;
import de.ostfalia.prog.ws21.enums.Farbe;
import de.ostfalia.prog.ws21.enums.Richtung;
import de.ostfalia.prog.ws21.exceptions.UngueltigePositionException;
import de.ostfalia.prog.ws21.felder.Dorf;
import de.ostfalia.prog.ws21.felder.Field;
import de.ostfalia.prog.ws21.felder.Pilz;

public class Schlumpfine extends Figur {

	public Schlumpfine(Farbe farbe, Field feld) {
		super(farbe, feld);
		this.iD = "Schlumpfine";

		// TODO Auto-generated constructor stub
	}
	
	public Schlumpfine(Field feld) {
		super(null, feld);
		this.iD = "Schlumpfine";

		// TODO Auto-generated constructor stub
	}

	@Override
	public void doNPCEvent(IO iO, int augenzahl) {
		for(Figur schlumpf : this.actualField.isHere) {
			if(!(this.actualField instanceof Pilz)) {
				if(schlumpf instanceof Schlumpf) {
					if(schlumpf.getisZombie()) {
						schlumpf.setisZombie(false);
						iO.print("schlumpfine healed: " + schlumpf.toString());
					}	
				}
			}
		}
		
	}
	@Override
	public boolean checkRestrictionsAndMove(int augenzahl, IO iO, Richtung richtung, Field field, List<Field> fields) {
			this.move(augenzahl, iO, richtung, field, fields);
			return true;
	}
	
	@Override
	public boolean goNextField(IO iO, int augenzahl, Richtung richtung , List<Field> fields) {
		
		this.actualField.checkForFieldEvent(iO, augenzahl, richtung, fields);		

		if(this.actualField != null && augenzahl > 0) {
			
			if(this.isRestricted(this.actualField.getnext())) {
				return false;
			}
			
			this.actualField.isHere.remove(this);
			
			//wenn im Dorf gehe zu 1
			if(this.actualField instanceof Dorf) {
				this.actualField = fields.get(1);
			} else {
				this.actualField = this.actualField.getnext();
			}
			
			this.actualField.isHere.add(this);
			
			iO.print(this.iD + " is here: " + this.actualField.pos);
		}
		return true;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.iD.toString();
	}

	@Override
	protected boolean isRestricted(Field field)  {
			for(Figur figur :field.isHere) {
				if(figur instanceof Fliege) {
					return true;
				}
			}
		return false;
	}
	

}
