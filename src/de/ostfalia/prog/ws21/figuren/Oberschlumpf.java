package de.ostfalia.prog.ws21.figuren;

import java.util.List;

import de.ostfalia.prog.ws21.IO;
import de.ostfalia.prog.ws21.enums.Farbe;
import de.ostfalia.prog.ws21.enums.Richtung;
import de.ostfalia.prog.ws21.exceptions.UngueltigePositionException;
import de.ostfalia.prog.ws21.felder.Field;

public class Oberschlumpf extends Figur {

	public Oberschlumpf(Field current) {
		this(null, current);
	}
	
	public Oberschlumpf(Farbe farbe, Field current) {
		super(farbe, current);
		
		this.iD = "Doc";
		this.fieldNumber = 29;
	}
	
	@Override
	public boolean checkRestrictionsAndMove(int augenzahl, IO iO, Richtung richtung, Field field, List<Field> fields) {
		this.move(augenzahl, iO, richtung, field, fields);
		return true;
	}
	
	public String toString() { 
		
		return this.iD + ":" + this.fieldNumber;
	}
	

	public void doNPCEvent(IO iO, int augenzahl) {
		for(Figur schlumpf : this.actualField.isHere) {
			if(schlumpf instanceof Schlumpf) {
				schlumpf.setisZombie(false);
			}
		}
	}

	@Override
	protected boolean isRestricted(Field field) {
		// TODO Auto-generated method stub
		return false;
	}
}
