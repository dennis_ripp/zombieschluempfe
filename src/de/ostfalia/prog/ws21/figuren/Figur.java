package de.ostfalia.prog.ws21.figuren;

import de.ostfalia.prog.ws21.enums.Farbe;
import de.ostfalia.prog.ws21.enums.Richtung;
import de.ostfalia.prog.ws21.exceptions.UngueltigePositionException;

import java.util.ArrayList;
import java.util.List;

import de.ostfalia.prog.ws21.IO;
import de.ostfalia.prog.ws21.felder.Dorf;
import de.ostfalia.prog.ws21.felder.Field;
import de.ostfalia.prog.ws21.felder.Fork;

public abstract class Figur {

	public Farbe figurFarbe;
	public String iD;
	public Field actualField;
	public int fieldNumber = 0;
	
	protected boolean isZombie;
	public Figur dummy;
	
	
	public Figur(Farbe farbe, Field feld){
		this.figurFarbe = farbe;
		this.actualField = feld;
		feld.isHere.add(this);
	}
	
	public void setisZombie(boolean set) {
		this.isZombie = set;
	}
	
	public boolean getisZombie() {
		return this.isZombie;
	}
	
    protected abstract boolean isRestricted(Field field);
    
    public abstract boolean checkRestrictionsAndMove(int augenzahl, IO iO, Richtung richtung, Field field, List<Field> fields);
    
    protected abstract void doNPCEvent(IO iO, int augenzahl);	
	
	public abstract String toString();
	
	protected boolean move(int augenzahl, IO iO, Richtung richtung, Field field, List<Field> fields) {
		if(augenzahl > 0) {
			this.goNextField(iO, augenzahl, richtung, fields);
			this.triggerNPCEventOnField(iO);
			this.doNPCEvent(iO, augenzahl);
			move(--augenzahl, iO, richtung, this.actualField, fields);
		}
		return true;
	}
	
	public void triggerNPCEventOnField(IO iO) {
		for(Figur figur : this.actualField.isHere)  {
			figur.doNPCEvent(iO, fieldNumber);
		}
	}
	public boolean goNextField(IO iO, int augenzahl, Richtung richtung , List<Field> fields) {
		if(this.actualField != null ) {	
				warpTo(this.actualField.getnext());
				iO.print(this.iD + " is here: " + this.fieldNumber);
		}			
		return true;
	}
	
	protected Field returnFieldDestination(int deepness, Richtung richtung) {
		Field feld = this.actualField;
		for(int i = 0; i < deepness; i++) {
			if(feld instanceof Fork && richtung == Richtung.ABZWEIGEN) {
				feld = ((Fork)feld).getalternatePath();
			} else {
				feld = feld.getnext();
			}
		}
		return feld;	
	}
	
	protected Field returnFieldDestination(int deepness, Richtung richtung, Field destination) {
		Field feld = destination;

		for(int i = 0; i < deepness; i++) {
			if(feld instanceof Fork && richtung == Richtung.ABZWEIGEN) {
				feld = ((Fork)feld).getalternatePath();
			} else {
				feld = feld.getnext();
			}
		}
		return feld;	
	}
	
	protected boolean checkFlyDorf(int deepness, Richtung richtung, Field feld) {
		if(returnFieldDestination(deepness, richtung, feld) instanceof Dorf) {
			return (istFliegeAufDemWeg(deepness, richtung));		
		} else {
			return false;
		}
	}
	
	protected boolean istFliegeAufDemWeg(int deepness, Richtung richtung) {
		Field feld = this.actualField;
		for(int i = 0; i < deepness; i++) {
			if(feld instanceof Fork && richtung == Richtung.ABZWEIGEN) {
				feld = ((Fork)feld).getalternatePath();
			} else {
				feld = feld.getnext();
			}
			for(Figur figur : feld.isHere) {
				if(figur instanceof Fliege) {
					return true;
				}
			}
		}
		return false;	
	}
	
	protected void warpTo(Field destination) {
		this.actualField.isHere.remove(this);
		this.actualField =  destination;
		this.actualField.isHere.add(this);
		this.fieldNumber = this.actualField.pos;
	}
	
}
