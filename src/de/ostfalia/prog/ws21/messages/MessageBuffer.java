package de.ostfalia.prog.ws21.messages;

import java.util.ArrayList;
import java.util.List;

public class MessageBuffer
{
    private List<Message> messages;

    public MessageBuffer() {
        messages = new ArrayList<Message>();
    }

    public void AddMessage(Message _msg) {
        messages.add(_msg);
    }

    public Message GetNext() {
        if (messages.size() <= 0) {
            return null;
        }
        Message msg = messages.get(0);
        messages.remove(0);
        return msg;
    }
}