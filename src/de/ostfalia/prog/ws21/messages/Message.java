package de.ostfalia.prog.ws21.messages;

public class Message {

	private String content;
	
	public Message() {
		
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	
}
